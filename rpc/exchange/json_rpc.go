package exchange

import (
	"context"

	"gitlab.com/kuzmem/gateway/pkg/processing/exchange"

	"gitlab.com/kuzmem/exchange/internal/modules/exchange/service"
)

type ExchangeServiceJSONRPC struct {
	exchangeService service.Exchanger
}

func NewExchangeServiceJSONRPC(exchangeService service.Exchanger) *ExchangeServiceJSONRPC {
	return &ExchangeServiceJSONRPC{exchangeService: exchangeService}
}

func (e ExchangeServiceJSONRPC) GetPriceListMax(in exchange.CryptoIn, out *exchange.CryptoOut) error {
	*out = e.exchangeService.GetPriceListMax(context.Background())
	return nil
}

func (e ExchangeServiceJSONRPC) GetPriceListMin(in exchange.CryptoIn, out *exchange.CryptoOut) error {
	*out = e.exchangeService.GetPriceListMin(context.Background())
	return nil
}

func (e ExchangeServiceJSONRPC) GetPriceListAvg(in exchange.CryptoIn, out *exchange.CryptoOut) error {
	*out = e.exchangeService.GetPriceListAvg(context.Background())
	return nil
}
