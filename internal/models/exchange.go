package models

import "encoding/json"

type CryptoMap map[string]Crypto

func UnmarshalCrypto(data []byte) (CryptoMap, error) {
	var r CryptoMap
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *CryptoMap) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Crypto struct {
	High float64 `json:"high,string"`
	Low  float64 `json:"low,string"`
}
