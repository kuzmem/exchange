package storages

import (
	"gitlab.com/kuzmem/exchange/internal/db/adapter"
	"gitlab.com/kuzmem/exchange/internal/modules/exchange/storage"
)

type Storages struct {
	ExMax storage.ExchangerMax
	ExMin storage.ExchangerMin
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		ExMax: storage.NewExchangeMax(sqlAdapter),
		ExMin: storage.NewExchangeMin(sqlAdapter),
	}
}
