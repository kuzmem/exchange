package service

import (
	"context"
	"encoding/json"
	"time"

	"go.uber.org/zap"

	amqp "github.com/rabbitmq/amqp091-go"
)

type Notifier interface {
	Push(in PushIn) error
}

type NotifySender struct {
	logger *zap.Logger
}

func NewNotifySender(logger *zap.Logger) Notifier {
	return &NotifySender{logger: logger}
}

func (n *NotifySender) Push(in PushIn) error {
	conn, err := amqp.Dial("amqp://guest:guest@notificator:5672/")
	if err != nil {
		n.logger.Error("Notifier connect err", zap.Error(err))
		return err
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		n.logger.Error("Notifier chan err", zap.Error(err))
		return err
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"rpc_queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		n.logger.Error("Notifier queue err", zap.Error(err))
		return err
	}

	send, err := json.Marshal(in)
	if err != nil {
		n.logger.Error("Notifier encode error", zap.Error(err))
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = ch.PublishWithContext(ctx,
		"",
		q.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        send,
		})
	if err != nil {
		n.logger.Error("Notifier publish err", zap.Error(err))
		return err
	}

	return nil
}

type PushIn struct {
	Pair    string  `json:"pair"`
	Message string  `json:"message"`
	Price   float64 `json:"price"`
}
