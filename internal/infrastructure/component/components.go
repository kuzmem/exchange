package component

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/kuzmem/exchange/config"
	"gitlab.com/kuzmem/exchange/internal/infrastructure/responder"
	"gitlab.com/kuzmem/exchange/internal/infrastructure/service"
	"gitlab.com/kuzmem/exchange/internal/infrastructure/tools/cryptography"
	"go.uber.org/zap"
)

type Components struct {
	Conf         config.AppConf
	Notifier     service.Notifier
	TokenManager cryptography.TokenManager
	Responder    responder.Responder
	Decoder      godecoder.Decoder
	Logger       *zap.Logger
	Hash         cryptography.Hasher
}

func NewComponents(conf config.AppConf, tokenManager cryptography.TokenManager, notifier service.Notifier, responder responder.Responder, decoder godecoder.Decoder, hash cryptography.Hasher, logger *zap.Logger) *Components {
	return &Components{Conf: conf, TokenManager: tokenManager, Notifier: notifier, Responder: responder, Decoder: decoder, Hash: hash, Logger: logger}
}
