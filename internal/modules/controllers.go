package modules

import (
	"gitlab.com/kuzmem/exchange/internal/infrastructure/component"
	"gitlab.com/kuzmem/exchange/internal/modules/exchange/controller"
)

type Controllers struct {
	Exchange controller.Exchanger
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	return &Controllers{
		Exchange: controller.NewExchange(services.Exchange, components),
	}
}
