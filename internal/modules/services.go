package modules

import (
	"gitlab.com/kuzmem/exchange/internal/infrastructure/component"
	"gitlab.com/kuzmem/exchange/internal/modules/exchange/service"
	"gitlab.com/kuzmem/exchange/internal/storages"
)

type Services struct {
	Exchange service.Exchanger
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{
		Exchange: service.NewExchangeService(*storages, components.Logger),
	}
}
