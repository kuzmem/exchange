package service

import (
	"context"
	"gitlab.com/kuzmem/exchange/internal/models"
	"gitlab.com/kuzmem/gateway/pkg/processing/exchange"
)

//go:generate easytags $GOFILE

type Exchanger interface {
	Upsert(ctx context.Context, in CryptoUpsertIn) CryptoUpsertOut
	GetPriceListMax(ctx context.Context) exchange.CryptoOut
	GetPriceListMin(ctx context.Context) exchange.CryptoOut
	GetPriceListAvg(ctx context.Context) exchange.CryptoOut
}

type CryptoUpsertIn struct {
	Currency models.CryptoMap
}

type CryptoUpsertOut struct {
	Success   bool
	ErrorCode int
}
