package storage

import (
	"context"

	"gitlab.com/kuzmem/exchange/internal/db/adapter"
	"gitlab.com/kuzmem/exchange/internal/infrastructure/db/scanner"
	"gitlab.com/kuzmem/exchange/internal/models"
)

type ExchangeMaxStorage struct {
	adapter *adapter.SQLAdapter
}

func NewExchangeMax(sqlAdapter *adapter.SQLAdapter) *ExchangeMaxStorage {
	return &ExchangeMaxStorage{adapter: sqlAdapter}
}

func (e *ExchangeMaxStorage) Upsert(ctx context.Context, m []models.ExchangeMaxDTO) error {
	in := make([]scanner.Tabler, len(m))
	for i, item := range m {
		copyItem := item
		in[i] = &copyItem
	}

	err := e.adapter.Upsert(ctx, in)

	return err
}

func (e *ExchangeMaxStorage) GetList(ctx context.Context) ([]models.ExchangeMaxDTO, error) {
	var out []models.ExchangeMaxDTO
	err := e.adapter.List(ctx, &out, "exchange_max", adapter.Condition{
		Order: []*adapter.Order{
			{
				Field: "pair",
				Asc:   true,
			},
		},
	})
	if err != nil {
		return nil, err
	}

	return out, nil
}
